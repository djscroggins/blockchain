# README #

This is a basic multithreaded Blockchain implementation completed for a course in distributed sytems.
Single file structure was a requirement for course submission. Intend to refactor to more proper multi-file structure.
It was built using Java version 1.8.0_72

##### To compile #####

```> javac Blockchain.java```

##### To run #####

This program is best run from a shell script that simultaneously starts several processes.

However, the program will run with a single process using the command:

```> java Blockchain```

The process number can be inputted on the command line, and will load associated input file automatically.

The following script will work if you are running the iTerm2 shell emulator on OSX. A similar structure will
work for most UNIX shell scripting:


```
tell application "iTerm2"
     create window with profile "BlockChain" command "java Blockchain 0"
     create window with profile "BlockChain" command "java Blockchain 1"
     create window with profile "BlockChain" command "java Blockchain 2"
end tell
```

Set up a profile to the working directory in iTerm (e.g. "BlockChain".
The flags indicate the process number.
Save with ".scpt" extension and run from the command line:

```> osascript bca.scpt```

##### File needed to run #####

* Blockchain.java
* BlockInput0.txt
* BlockInput1.txt
* BlockInput2.txt
