/*-----------------------------------------------

1. David Scroggins / February 14, 2018

2. java version 1.8.0_72

3. To compile:
> javac Blockchain.java

4. To run:
This program is best run from a shell script that simultaneously starts several processes.

However, the program will run with a single process using the command:

> java Blockchain

The process number can be inputted on the command line, and will load associated input file automatically.

The following script will work if you are running the iTerm2 shell emulator on OSX. A similar structure will
work for most UNIX shell scripting:

tell application "iTerm2"
     create window with profile "BlockChainAssignment" command "java Blockchain 0"
     create window with profile "BlockChainAssignment" command "java Blockchain 1"
     create window with profile "BlockChainAssignment" command "java Blockchain 2"
end tell

Set up a profile to the working directory in iTerm (e.g. "BlockChainAsssignment".
Save with ".scpt" extension and run from the command line:

> osascript bca.scpt


5. Files needed to run:
Blockchain.java
BlockInput0.txt
BlockInput1.txt
BlockInput2.txt

6. Notes:

----------------------------------------------- */

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;


public class Blockchain {

    // These booleans track whether servers have initialized
    // Used to ensure all threads and processes are synced up
    volatile static boolean areKeyServersStarted = false;
    volatile static boolean arePublicKeysReceived = false;
    volatile static boolean areBlockServersStarted = false;
    volatile static boolean areBlockChainServersStarted = false;
    volatile static boolean isInitialized = false;
    static String serverName = "localhost";
    static int numProcesses = 3;
    // Internally generates and holds public/private key pairs
    // See class below for methods
    static KeyHolder keyHolder = new KeyHolder();
    // Basic synchronized List to hold the block records
    static List<BlockRecord> blockChain = Collections.synchronizedList(new ArrayList<BlockRecord>());
    // Set containing UUID strings that have been inserted into the block chain
    static Set<String> chainIds = Collections.synchronizedSet(new HashSet<String>());
    // HashMap for retrieving public keys by process id
    static ConcurrentHashMap<Integer, PublicKey> publicKeys = new ConcurrentHashMap<>();
    // Priority queue for storing unverified block records
    static BlockingQueue<BlockRecord> unverifiedRecordsQueue = new PriorityBlockingQueue<>();
    // Difficulty level for work problem. Number indicates how many leading zeros the solution must contain
    // Difficulty of five can be solved in seconds (sometimes under a second) on 2.5 GHz Intel Core i7
    static int difficulty = 5;

    public static void main(String[] args) {
        int queueLength = 6;
        int processId;
        Socket initSocket;

        processId = (args.length < 1) ? 0 : Integer.parseInt(args[0]);

        // Setting up ports for process
        // See Ports class for more detail
        Ports ports = new Ports();
        int initializationPort = ports.getInitializationPort(processId);
        int publicKeyPort = ports.getPublicKeyPort(processId);
        int unverifiedBlockPort = ports.getUnverifiedBlockPort(processId);
        int updatedBlockChainPort = ports.getUpdatedBlockChainPort(processId);

        // Start up console info
        System.out.println("Starting up Process " + processId + " with difficulty level: " + difficulty);
        System.out.println("Initialization Port: " + initializationPort);
        System.out.println("Public Key Port: " + publicKeyPort);
        System.out.println("Unverified Block Port: " + unverifiedBlockPort);
        System.out.println("Updated Blockchain Port: " + updatedBlockChainPort);

        new Thread(new InitializationListener(initializationPort)).start();

        initializeBlockChain();

        // Starting up server for public keys
        new Thread(new PublicKeyServer(processId, publicKeyPort, queueLength)).start();

        // Wait for server start up acknowledgement
        while (!areKeyServersStarted);

        System.out.println("\nInitializing Public Keys ....");
        // multicast public keys to other proccesses
        sendPublicKeys(processId, keyHolder);

        // Wait for public key receipt
        while (!arePublicKeysReceived);

        // Starting up server for unverified blocks
        new Thread(new UnverifiedBlockServer(processId, unverifiedBlockPort, queueLength)).start();

        // Wait for server start up acknowledgement
        while (!areBlockServersStarted);

        System.out.println("\nInitializing Unverified Blocks ....");

        // For initialization just build string dynamically based on command line input
        String inputFile = "BlockInput" + Integer.toString(processId) + ".txt";
        sendUnverifiedBlocks(inputFile, processId);

        while (!areBlockChainServersStarted);

        // Start up Blockchain servers
        new Thread(new BlockChainServer(processId, updatedBlockChainPort, queueLength)).start();

        // Wait for initialization message
        while (!isInitialized);

        System.out.println("\nInitialized. Now verifying blocks ...");

        // Start verifying blocks
        new Thread(new BlockVerifier(processId)).start();

        printInstructions();

        scanForConsoleInput(processId, unverifiedBlockPort);

    }

    /**
     * Initializes bock chain with simple genesis node
     */
    private static void initializeBlockChain () {
        BlockRecord genesisBlock = new BlockRecord();
        genesisBlock.setBlockUUID("0");
        genesisBlock.setPreviousHash("genesis");
        String stringTimeStamp = "0000000000000";
        genesisBlock.setTimeStamp(stringTimeStamp);
        genesisBlock.setHash();
        genesisBlock.setBlockSequence(0);
        blockChain.add(genesisBlock);
        chainIds.add(genesisBlock.getBlockUUID());
    }

    /**
     * Methods receives name of file for input as well as current process id.
     * After calling auxiliary method to load objects from file, marshals objects
     * to XML string and multicasts them to other running processes
     * @param inputFilein string name of file inputted
     * @param processIdIn current process id
     */
    private static void sendUnverifiedBlocks (String inputFilein, int processIdIn) {
        Socket sock = null;
        int port;
        PrintStream toServer = null;
        ArrayList<BlockRecord> blockRecordsArrayList;
        String processIdString = Integer.toString(processIdIn);

        blockRecordsArrayList = loadObjectsFromInput(inputFilein, processIdIn, processIdString);

        System.out.println(blockRecordsArrayList.size() + " records loaded from input.");

        // Marshalling to xml
        try {

            // Establishes context for accessing the JAXB API
            // In this case initialized for BlockRecord class
            JAXBContext jaxbContext = JAXBContext.newInstance(BlockRecord.class);

            // Nested for loops:
            // For each BlockRecord:
            // 1) Marshall to XML, 2) send record to each UnverifiedBlockServer
            for (BlockRecord blockRecord : blockRecordsArrayList) {

                // Marshaller converts Java content tree (see BlockRecord class) to XML
                Marshaller marshaller = jaxbContext.createMarshaller();
                // StringWriter into which we'll writ our XML
                StringWriter stringWriter = new StringWriter();
//                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                // Marshal individual BlockRecord instance to stringWriter instance
                marshaller.marshal(blockRecord, stringWriter);
                // Return single string from characters in stringWriter
                // Content at this point should represent XML structure for single block record
                String stringRecordXML = stringWriter.toString();

                // Print XML string to each of the running Unverified Block Servers
                for (int i=0; i < numProcesses; i++) {
                    try {
                        int currentPort = Ports.getUnverifiedBlockPortBase() + i;
                        sock = new Socket(serverName, currentPort);
                        toServer = new PrintStream(sock.getOutputStream());
                        toServer.println(stringRecordXML);
                    } catch (Exception e) {
                        System.out.println("There was an issue sending Unverified Blocks to server(s).");
                        e.printStackTrace();
                    } finally {
                        if (sock != null) {sock.close();}
                        if (toServer != null) {toServer.close();}

                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error marshalling objects");
            e.printStackTrace();
        }
    }

    /**
     * Reads in named input file and loads contents to BlockRecord instance (Java content tree). Once
     * file is empty, returns on ArrayList of records read to BlockRecord instances
     * @param fileNameIn string name of file
     * @param processIdIn current process id
     * @param processIdStringIn string version of process id (simplifies hashing operations)
     * @return ArrayList of BlockRecords
     */
    private static ArrayList<BlockRecord> loadObjectsFromInput(String fileNameIn, int processIdIn, String processIdStringIn) {

        ArrayList<BlockRecord> blockRecordArrayList = new ArrayList<>();

        try {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileNameIn))) {

                // Array to hold split line contents (individual record fields)
                String[] recordFields = new String[10];
                // String to read line into
                String inputLinesString;

                // To track index position in ArrayList
                int i = 0;

                // While the file has content to read, read it in to buffer
                // the read each line to a string for splitting
                while ((inputLinesString = bufferedReader.readLine()) != null) {

                    blockRecordArrayList.add(i, new BlockRecord());

                    String uuidString = UUID.randomUUID().toString();
                    blockRecordArrayList.get(i).setBlockUUID(uuidString);
                    // Sign uuidString with private key (see Utilities class for more details)
                    byte[] signed = Utilities.signData(uuidString, keyHolder.getMyPrivateKey());
                    blockRecordArrayList.get(i).setSignedUUID(signed);
                    blockRecordArrayList.get(i).setCreatingProcess(processIdStringIn);
                    // blockSequence set when added to chain
                    // verifying block set when verified
                    // begin adding content from file input
                    recordFields = inputLinesString.split(" +");
                    blockRecordArrayList.get(i).setFirstName(recordFields[0]);
                    blockRecordArrayList.get(i).setLastName(recordFields[1]);
                    blockRecordArrayList.get(i).setDateOfBirth(recordFields[2]);
                    blockRecordArrayList.get(i).setSSN(recordFields[3]);
                    blockRecordArrayList.get(i).setDiagnosis(recordFields[4]);
                    blockRecordArrayList.get(i).setTreatment(recordFields[5]);
                    blockRecordArrayList.get(i).setPrescription(recordFields[6]);
                    // add final properties
                    // previous hash is set popped from queue
                    // Time stamp in milliseconds
                    String newTimeStamp = Long.toString(new Date().getTime());
                    blockRecordArrayList.get(i).setTimeStamp(newTimeStamp);
                    blockRecordArrayList.get(i).setHash();
                    // Time stamp is in millisecond; this slows the threads down just enough to
                    // ensure spaced out timestamps
                    Thread.sleep(100);
                    // nonce set when block puzzle is solved
                    i++;
                }
            } catch (Exception e) {
                System.out.println("Reader threw error in loadObjectsFromInput");
                e.printStackTrace();
            }
        } catch (Exception e) {
            System.out.println("Something failed in loadObjectsFromInput");
            e.printStackTrace();
        }
        return blockRecordArrayList;
    }

    /**
     * Sends process's public key to other running processes
     * @param processIdIn current process id
     * @param keyHolderIn KeyHolder instance for getting current process's public key
     */
    private static void sendPublicKeys (int processIdIn, KeyHolder keyHolderIn) {
        Socket sock = null;
        ObjectOutputStream objToServer = null;

        // Loads process id and public key in byte form into serialized container to be sent via socket
        KeyContainer keyContainer = new KeyContainer(processIdIn, keyHolderIn.getPublicKeyEncoded());

        int myPort = Ports.getPublicKeyBasePort() + processIdIn;

        // Send keys to all processes
        for (int i=0; i < numProcesses; i++) {
            try {
                // Increment through ports starting with base port up to number of processes running
                int currentPort = Ports.getPublicKeyBasePort() + i;
                sock = new Socket(serverName, currentPort);
                objToServer = new ObjectOutputStream(sock.getOutputStream());
                objToServer.writeObject(keyContainer);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (sock != null) { try { sock.close(); } catch (Exception e) { e.printStackTrace(); }
                if (objToServer != null) { try { objToServer.close(); } catch (Exception e) { e.printStackTrace(); }
                    }
                }
            }
        }
    }

    private static void printInstructions () {
        System.out.print("\nEnter <C> to see count of how many blocks each process has verified\n" +
                "Enter <R> to read in a new file as unverified blocks\n" +
                "Enter <V> to verify blockchain\n"
                + "Enter <V flags> to see verification options\n" +
                "Enter <L> to list records in blockchain\n" + "Enter command: ");
    }

    /**
     * Processes console input from user
     * @param processIdIn current process id
     * @param unverifiedBlockPortIn port for sending unverified input blocks to
     */
    private static void scanForConsoleInput (int processIdIn, int unverifiedBlockPortIn) {
        Scanner userInput = new Scanner(System.in);

        while (true) {
            System.out.flush();
            String entry = userInput.nextLine();

            if (entry.equals("")) {
                printInstructions();
            } else if (entry.matches("(?i)\\bc\\b")) {
                countProcessVerifications();
                printInstructions();
            } else if (entry.toLowerCase().startsWith("r")) {
                try {
                    String[] input = entry.split("\\s+");
                    if (!input[1].endsWith(".txt")) {
                        throw new IllegalArgumentException();
                    } else {
                        System.out.println("Reading in new file ...");
                        sendUnverifiedBlocks(input[1], processIdIn);
                        printInstructions();
                    }
                } catch (ArrayIndexOutOfBoundsException aiobe) {
                    System.out.println("\nEntry must be in precise form: <r> <filename>. Make sure you included a space.");
                    printInstructions();
                } catch (IllegalArgumentException iae) {
                    System.out.println("\nOnly .txt file input supported.\n Double check input format and command spacing.");
                    printInstructions();
                }
            } else if (entry.matches("(?i)\\bv\\b")) {
                System.out.println("\nVerifying blockchain...");
                boolean verification = verifyBlockChain(0);
                printInstructions();
            } else if (entry.toLowerCase().equals("v flags")) {
                printFlagInstructions();
            } else if (entry.toLowerCase().equals("v hash")) {
                verifyBlockChain(1);
                printInstructions();
            } else if (entry.toLowerCase().equals("v links")) {
                verifyBlockChain(2);
                printInstructions();
            } else if (entry.toLowerCase().equals("v signature")) {
                verifyBlockChain(3);
                printInstructions();
            } else if (entry.toLowerCase().equals("v work")) {
                verifyBlockChain(4);
                printInstructions();
            } else if (entry.matches("(?i)\\bl\\b")) {
                System.out.println("\nListing records...");
                listCurrentVerifiedRecords();
                printInstructions();
            } else {
                printInstructions();
            }
        }
    }

    /**
     * Prints flag instructions for block chain verification
     */
    private static void printFlagInstructions () {
        System.out.print("\n V with no flag verifies the blockchain by: " +
                "\n  1) Checking if the block's matches the expected hash value" +
                "\n  2) Checking if the block's previous  is equivalent to the hash of the previous block" +
                "\n  3) Checking if the verifying block's signature of the header is valid" +
                "\n  4) Checking of the block's hash meets the required work threshold" +
                "\nIf you prefer a lighter verification process append one of the following flags (which correspond to 1-4 above)" +
                "\nE.g. V hash" +
                "\n\nFlags: hash, links, signature, work"
                + "\n\nPress <enter> to exit this menu:");
    }

    /**
     * Totals and displays number of blocks each process has verified
     */
    private static void countProcessVerifications() {

        // TreeMaps are a perfect structure for this since they provide easy key/value retrieval
        // but also guarantee iteration order; value more evident with higher process counts
        TreeMap<String, Counter> processCounts = new TreeMap<>();

        // Load Tree Map
        for (int i = 0; i < numProcesses; i++) {
            processCounts.put(Integer.toString(i), new Counter());
        }

        // Loop through block chain and increment counts for each process
        for (BlockRecord br : blockChain) {

            String verifyingProcess = br.getVerifyingProcess();

            // This check is necessary, because if we run this functionality early before records
            // have been processed, then the above line might return null causing the TreeMap access
            // to throw an exception
            if (verifyingProcess != null) {
                processCounts.get(br.getVerifyingProcess()).incrementCount();
            }
        }

        System.out.println("\nVerified block counts by process:");
        processCounts.forEach((k, v) -> System.out.print("P" + k + "=" + v.getCount() + " "));
        System.out.println();


    }

    /**
     * Verifies the block chain. By default using four factors: hash comparison, current hash to previous hash
     * comparison, validate signature of header, validate solution. Flags allow for more fine-grained verification.
     * @return boolean
     */
    // TODO: I want to make this a little finer grain to allow for flags (if I have time)
    private static Boolean verifyBlockChain (int optionIn) {
        BlockRecord currentBlock;
        BlockRecord previousBlock;

        // Note: I know these work, because they helped me catch a couple of small errors in logic that were causing
        // occasional bugs
        for (int i = 1; i < blockChain.size(); i++) {
            currentBlock = blockChain.get(i);
            previousBlock = blockChain.get(i - 1);

            if ((optionIn == 0 || optionIn == 1) && !currentBlock.getHash().equals(currentBlock.calculateHash())) {
                System.out.println("Block " + currentBlock.getBlockSequence() + " is invalid: SHA256 hashes do not match");
                System.out.println("Current hash: " + currentBlock.getHash() + " Calculated hash: " + currentBlock.calculateHash());
                return false;
            }

            if ((optionIn == 0 || optionIn == 2) && !previousBlock.getHash().equals(currentBlock.getPreviousHash())) {
                System.out.println("Block " + currentBlock.getBlockSequence() + " is invalid: Current hash and previous block hash do not match.");
                return false;
            }

            if ((optionIn == 0 || optionIn == 3) && !checkHeaderSignature(currentBlock)) {
                System.out.println("Block " + currentBlock.getBlockSequence() + " is invalid: Signed header does not match unsigned hash.");
                return false;
            }

            if ((optionIn == 0 || optionIn == 4) && !solutionIsValid(currentBlock)) {
                System.out.println("Block " + currentBlock.getBlockSequence() +  " is invalid: Hash does not meet work threshold");
                return false;
            }
        }
        System.out.println("The Blockchain has been verified.");
        return true;
    }

    /**
     * Verifies BlockRecord header signature
     * @param blockRecordIn a BlockRecord instance
     * @return boolean
     */
    private static boolean checkHeaderSignature (BlockRecord blockRecordIn) {
        Integer processId = Integer.parseInt(blockRecordIn.getVerifyingProcess());
        PublicKey publicKey = Blockchain.publicKeys.get(processId);
        String unsignedHash = blockRecordIn.getHash();
        byte[] signedHeader = blockRecordIn.getHeader();
        return Utilities.verifySignature(publicKey, unsignedHash, signedHeader);
    }

    /**
     * Displays summary data for current records in block chain
     */
    private static void listCurrentVerifiedRecords() {

        for (int i = blockChain.size() - 1; i >= 0; i--) {
            int sequence = blockChain.get(i).getBlockSequence();
            String timestamp = blockChain.get(i).getTimeStamp();
            String firstName = blockChain.get(i).getFirstName();
            String lastName = blockChain.get(i).getLastName();
            String SSN = blockChain.get(i).getSSN();
            String diagnosis = blockChain.get(i).getDiagnosis();
            String treatment = blockChain.get(i).getTreatment();
            String prescription = blockChain.get(i).getPrescription();

            System.out.println("\n" + " " + sequence + ": " + timestamp + " " + firstName + " " + lastName + " " +
                    SSN + " " + diagnosis + " " + treatment + " " + prescription);
        }
    }

    /**
     * Checks to make sure solution satisfies difficulty level
     * @param blockRecordIn BlockRecord instance
     * @return boolean
     */
    private static boolean solutionIsValid(BlockRecord blockRecordIn) {
        // Generates string of zeros of length=difficulty
        String target = new String(new char[difficulty]).replace('\0', '0');
        // If hash begins with N zeros where N=difficulty returns true
        return blockRecordIn.getHash().substring(0, difficulty).equals(target);
    }

}

/**
 * Class to verify generated blocks
 */
class BlockVerifier implements Runnable {

    private int processId;

    BlockVerifier (int processIdIn) {
        processId = processIdIn;
    }

    @Override
    public void run() {

        verifyBlock();

    }

    /**
     * Takes unverified record from priority queue, validates it, then begins work process.
     * After completion of work updates block and broadcast it to BlockChainServer(s)
     */
    private void verifyBlock () {
        try {
            while (true) {

                // Current blockRecord with earliest creation timestamp
                BlockRecord blockRecord = Blockchain.unverifiedRecordsQueue.take();
                // See method for more details, but checks if in block chain and validates creation signature
                if (isValid(blockRecord)) {

                    // This needs to be set prior to doing the work so that it incorporated into the new hash
                    blockRecord.setPreviousHash(Blockchain.blockChain.get(Blockchain.blockChain.size()-1).getHash());


                    boolean isMined = blockRecord.mineBlock(Blockchain.difficulty);

                    // If block is successfully mined do the following
                    if (isMined) {

                        boolean inChain = Utilities.checkIfInBlockChain(blockRecord);

                        // If block not in chain, then append and send
                        if (!inChain) {
                            // This is a bit ugly, but was the immediate fix I needed to avoid concurrency exceptions when accessing my
                            // ArrayList. I might want to switch to a Copy on Write array list or further explore customizing this
                            // data structure, but for present purposes, synchronizing this block solves the problem.
                            synchronized (Blockchain.blockChain) {
                                blockRecord.setBlockSequence(Blockchain.blockChain.get(Blockchain.blockChain.size() - 1).getBlockSequence() + 1);
                                blockRecord.setVerifyingProcess(Integer.toString(processId));
                                String hash = blockRecord.getHash();
                                blockRecord.setHeader(Utilities.signData(hash, Blockchain.keyHolder.getMyPrivateKey()));
                                Blockchain.blockChain.add(blockRecord);
                                // Add to set tracking blocks in Blockchain
                                Blockchain.chainIds.add(blockRecord.getBlockUUID());
                                // Put blockchain in container to send to BlockChainServer
                                BlockChainContainer blockChainContainer = new BlockChainContainer(Blockchain.blockChain, Blockchain.chainIds);
                                sendBlock(blockChainContainer);

                            }

                            // Randomly slow down execution by throwing in a litle noise; I did this to simulate uneven processing power
                            Thread.sleep(ThreadLocalRandom.current().nextInt(100, 500));
                        }
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Take operation was interrupted in BlockVerifier");
            e.printStackTrace();
        }
    }

    /**
     * Send block to all running BlockChainServers
     * @param blockChainContainerIn container with block chain and chain ids to be updated
     */
    private void sendBlock (BlockChainContainer blockChainContainerIn) {

        ObjectOutputStream objToServer = null;
        Socket sock = null;

        for (int i = 0; i < Blockchain.numProcesses; i++) {
            try {
                int currentPort = Ports.getUpdatedBlockChainPortBase() + i;
                sock = new Socket(Blockchain.serverName, currentPort);
                objToServer = new ObjectOutputStream(sock.getOutputStream());
                objToServer.writeObject(blockChainContainerIn);
            } catch (Exception e) {
                System.out.println("Error sending blocks");
                e.printStackTrace();
            } finally {
                if (sock != null) { try { sock.close(); } catch (Exception e) { e.printStackTrace(); } }
                if (objToServer != null) { try { objToServer.close(); } catch (Exception e) { e.printStackTrace(); }
                }
            }
        }
    }

    /**
     * Checks if block record is in block chain and if its signature is valid
     * @param blockRecordIn BlockRecord to be validated
     * @return boolean
     */
    private boolean isValid(BlockRecord blockRecordIn) {

        return !Utilities.checkIfInBlockChain(blockRecordIn) && checkUUIDSignature(blockRecordIn);
    }

    /**
     * Helper method to validate block record's signature
     * @param blockRecordIn BlockRecord to be validated
     * @return boolean
     */
    private boolean checkUUIDSignature (BlockRecord blockRecordIn) {
        Integer processId = Integer.parseInt(blockRecordIn.getCreatingProcess());
        PublicKey publicKey = Blockchain.publicKeys.get(processId);
        String unsignedUUIDString = blockRecordIn.getBlockUUID();
        byte[] signedUUIDBytes = blockRecordIn.getSignedUUID();
        return Utilities.verifySignature(publicKey, unsignedUUIDString, signedUUIDBytes);
    }
}

/**
 * Serialized container for shipping new blockChains and chainIds to servers
 */
class BlockChainContainer implements Serializable {

    private List<BlockRecord> blockChain = Collections.synchronizedList(new ArrayList<BlockRecord>());
    private Set<String> chainIds = Collections.synchronizedSet(new HashSet<String>());

    BlockChainContainer (List<BlockRecord> blockChainIn, Set<String> chainIdsIn) {
        blockChain = blockChainIn;
        chainIds = chainIdsIn;
    }

    public List<BlockRecord> getBlockChain() {
        return blockChain;
    }

    public Set<String> getChainIds() {
        return chainIds;
    }
}

/**
 * Class for block records with XML mapping for marshalling, unmarshalling and
 * writing to disk. Implements comparable based on time stamp field and
 * also implements Serializable so it can be included and shipped in
 * block chain containers
 */
@XmlRootElement
class BlockRecord implements Comparable<BlockRecord>, Serializable {
    private byte[] Header;
    private String blockUUID;
    private byte[] signedUUID;
    private String creatingProcess;
    private int blockSequence;
    private String verifyingProcess;
    private long verifyingProcessTimeStamp;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String SSN;
    private String diagnosis;
    private String treatment;
    private String prescription;
    @XmlElement
    private String hash;
    private String previousHash;
    private String timeStamp;
    private int nonce;

    public byte[] getHeader() {
        return Header;
    }

    @XmlElement
    public void setHeader(byte[] header) {
        Header = header;
    }

    public String getBlockUUID() {
        return blockUUID;
    }

    @XmlElement
    public void setBlockUUID(String blockUUID) {
        this.blockUUID = blockUUID;
    }

    public byte[] getSignedUUID() {
        return signedUUID;
    }

    @XmlElement
    public void setSignedUUID(byte[] signedUUID) {
        this.signedUUID = signedUUID;
    }

    public String getCreatingProcess() {
        return creatingProcess;
    }

    @XmlElement
    public void setCreatingProcess(String creatingProcess) {
        this.creatingProcess = creatingProcess;
    }

    public int getBlockSequence() {
        return blockSequence;
    }

    @XmlElement
    public void setBlockSequence(int blockSequence) {
        this.blockSequence = blockSequence;
    }

    public String getVerifyingProcess() {
        return verifyingProcess;
    }

    @XmlElement
    public void setVerifyingProcess(String verifyingProcess) {
        this.verifyingProcess = verifyingProcess;
    }

    public String getFirstName() {
        return firstName;
    }

    @XmlElement
    public void setVerifyingProcessTimeStamp(long verifyingProcessTimeStamp) {
        this.verifyingProcessTimeStamp = verifyingProcessTimeStamp;
    }

    public long getVerifyingProcessTimeStamp() {
        return verifyingProcessTimeStamp;
    }

    @XmlElement
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @XmlElement
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @XmlElement
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSSN() {
        return SSN;
    }

    @XmlElement
    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    @XmlElement
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getTreatment() {
        return treatment;
    }

    @XmlElement
    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getPrescription() {
        return prescription;
    }

    @XmlElement
    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getHash() {
        return hash;
    }

//    @XmlElement
    public void setHash() {
        this.hash = calculateHash();
    }

    public String getPreviousHash() {
        return previousHash;
    }

    @XmlElement
    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    @XmlElement
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getNonce() {
        return nonce;
    }

    @XmlElement
    public void setNonce(int nonce) {
        this.nonce = nonce;
    }

    /**
     * Method to apply SHA256 hash to block record. Note, setHash() calls this
     * method internally. Depends on Utility class.
     * @return SHA256 hash of record's data
     */
    public String calculateHash () {
        String fieldData = firstName + lastName + dateOfBirth + diagnosis + treatment + prescription;
        return Utilities.applySHA256(previousHash + fieldData + timeStamp + Integer.toString(nonce));
    }

    /**
     * Method to complete work needed to verify block
     * @param difficultyIn difficulty level for puzzle i.e. number of leading 0's needed in solution
     * @return boolean indicating success or failure
     */
    public boolean mineBlock (int difficultyIn) {
        // Creating target of N 0's where N=difficultyIn
        String target = new String(new char[difficultyIn]).replace('\0', '0');
        // Get current number of nodes in block chain
        int blockChainSizeOnEntry = Blockchain.blockChain.size();

        // Iterate until first N characters of hash equal zero where N=difficultyIn
        while (!hash.substring(0, difficultyIn).equals(target)) {
            // Recall that nonce is included in in calculateHash as unique stamp of how much work was done
            nonce++;
            // We constantly recalculate the hash as we increment the nonce until it equals the target
            hash = calculateHash();

            // Check for updates every 100 iterations
            // Note: even at low difficulty level total iterations are almost always 100,000+
            // So this is a pretty frequent check, but I wanted to err on side of caution for the assignment.
            if (nonce % 100 == 0) {

                // Check if this block now in block chain; if it is, method
                // returns false to calling method and block is discarded
                if (Blockchain.chainIds.contains(this.getBlockUUID())) {
                    return false;
                // If current block chain size is larger than size on method entry
                // but this block has not been added to the block chain, update block
                // fields and restart work
                } else if (Blockchain.blockChain.size() > blockChainSizeOnEntry) {
                    // Reset nonce since we have to restart work
                    nonce = 0;
                    // Update previous hash field to include hash of most recently inserted block
                    // This change is important or it will invalidate the block
                    this.setPreviousHash(Blockchain.blockChain.get(Blockchain.blockChain.size()-1).hash);
                    // Update block chain size and continue iterating
                    blockChainSizeOnEntry = Blockchain.blockChain.size();
                }
            }
        }

        // Timestamp for process collisions. See BlockChainServer for more details, but it is possible with
        // this current implementation to generate two separate valid block chains where two blocks with identical
        // UUIDs are actually validated by two separate processes. Since I'm not dealing with the more advanced
        // features, I used this to resolve collisions between processes and ensure identical block chains in all
        // processes
        this.setVerifyingProcessTimeStamp(new Date().getTime());
        return true;
    }

    @Override
    public int compareTo(BlockRecord other) {
        int lastCmp = timeStamp.compareTo(other.timeStamp);
        return lastCmp != 0 ? lastCmp : timeStamp.compareTo(other.timeStamp);
    }
}

/**
 * Utility class for general methods needed widely in process
 */
class Utilities {

    /**
     * Applies SHA256 hash to string
     * @param inputIn string to be hashed
     * @return hashed string in hexadecimal
     */
    static String applySHA256 (String inputIn) {
        try {
            // MessageDigest class allows one to take in arbitrary length string
            // and output a fixed length hash. In this case we get a SHA-256 instance
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            // Applies hash to input and returns byte array of resulting hash value
            byte[] hash = digest.digest(inputIn.getBytes("UTF-8"));

            // Converts hash to hex
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Signs string data with provided private key
     * @param stringIn string for signing
     * @param privateKeyIn inputted private key
     * @return byte array of signed data
     */
    static byte [] signData (String stringIn, PrivateKey privateKeyIn) {

        try {
            // Convert string to bytes
            byte[] stringBytes = stringIn.getBytes();
            // Get algorithm instance
            Signature signer = Signature.getInstance("SHA1withRSA");
            // Initializes given object for signing; can throw InvalidKeyException
            signer.initSign(privateKeyIn);
            // This is the data to be signed
            signer.update(stringBytes);
            // Signs updated data
            return (signer.sign());

        } catch (Exception e) {
            System.out.println("Something went wrong in signData");
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    /**
     * Verifies signed data using public key
     * @param publicKeyIn provided public key
     * @param stringIn unsigned string for verification
     * @param signatureIn signed data to be verified
     * @return boolean
     */
    static boolean verifySignature (PublicKey publicKeyIn, String stringIn, byte[] signatureIn) {
        try {
            // Get algorithm instance
            Signature signature = Signature.getInstance("SHA1withRSA");
            // Initialize with given public key
            signature.initVerify(publicKeyIn);
            // Update with inputted unsigned string
            signature.update(stringIn.getBytes());
            // Verify whether data was signed with matching private key
            return signature.verify(signatureIn);
        } catch (Exception e) {
            System.out.println("Something went wrong in verifySignature");
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks if given BlockRecord is in the block chain
     * @param blockRecordIn block record
     * @return boolean
     */
    static boolean checkIfInBlockChain(BlockRecord blockRecordIn) {
        return Blockchain.chainIds.contains(blockRecordIn.getBlockUUID());
    }

    /**
     * Sends string acknowledgement message to initialization servers
     * @param messageIn string message to send
     */
    static void sendAcknowledgement (String messageIn) {
        Socket socket = null;
        PrintStream toPeer = null;


        for (int i = 0; i < Blockchain.numProcesses; i++) {
            try {
                int currentPort = Ports.getInitializationBasePort() + i;
                socket = new Socket(Blockchain.serverName, currentPort);
                toPeer = new PrintStream(socket.getOutputStream());
                toPeer.println(messageIn);

            } catch (Exception e) {
                System.out.println("Error sending initialization message");
                e.printStackTrace();
            } finally {
                if (socket != null) { try { socket.close(); } catch (Exception e) { e.printStackTrace();}}
                if (toPeer != null) { toPeer.close();}
            }
        }
    }
}

/**
 * Server class for receiving updated block chains and processing them
 */
class BlockChainServer implements Runnable {

    private int processId;
    private int port;
    private int queueLength;

    BlockChainServer (int processIdIn, int portIn, int queueLengthIn) {
        processId = processIdIn;
        port = portIn;
        queueLength = queueLengthIn;
    }

    class BlockChainWorker extends Thread {

        private int processId;
        private Socket sock;

        /**
         * Inner class responsible for processing received block chains
         * @param processIdIn inputted process id
         * @param sockIn inputted server socket
         */
        BlockChainWorker (int processIdIn, Socket sockIn) {
            processId = processIdIn;
            sock = sockIn;
        }


        public void run() {

            try (ObjectInputStream objIn = new ObjectInputStream(sock.getInputStream());) {

                // Received object containing updated block chain and ids set
                BlockChainContainer blockChainContainer = (BlockChainContainer) objIn.readObject();

                // Terminal block of received block chain
                BlockRecord receivedTerminalBlock = blockChainContainer.getBlockChain().get(blockChainContainer.getBlockChain().size()-1);
                // Terminal block of current un-updated block chain
                BlockRecord currentTerminalBlock = Blockchain.blockChain.get(Blockchain.blockChain.size() - 1);

                // Size of received block chain (size() returns number of elements)
                int receivedLength = blockChainContainer.getBlockChain().size();
                // Size of current block chain
                int currentLength = Blockchain.blockChain.size();

                int sizeComparison = receivedLength - currentLength;

                // If received block chain is 1 longer (expected value) and if terminal block not in block chain
                // then update process block chain with received chain
                if (sizeComparison == 1 && !Utilities.checkIfInBlockChain(receivedTerminalBlock)) {
                    Blockchain.blockChain = blockChainContainer.getBlockChain();
                    Blockchain.chainIds = blockChainContainer.getChainIds();
                }

                // If received block chain same length, but block isn't in block chain, send it back for
                // re-verification. This condition isn't likely to occur, nor are blocks likely to be lost
                // in this small of an application, but just in case
                else if (sizeComparison == 0 && !Utilities.checkIfInBlockChain(receivedTerminalBlock)) {
                    sendBlockBack(receivedTerminalBlock);
                // If received block chain the same length, but the terminal blocks have the same UUID
                // I included notes in for this in the BlockRecord class mineBlock() method, but it is possible to
                // produce valid block chains in separate processes that are identical except that the blocks were
                // verified by different processes (i.e. they will not fail the validation tests).
                // Since I'm not dealing with the more complicated forking components
                // for extra credit, I included the following logic to resolve this bug in my implementation
                } else if (sizeComparison == 0 && receivedTerminalBlock.getBlockUUID().equals(currentTerminalBlock.getBlockUUID())) {
                    // If the blocks were not verified by the same process, compare their timestamps. Block that was verified
                    // first wins
                    if (!receivedTerminalBlock.getVerifyingProcess().equals(currentTerminalBlock.getVerifyingProcess())) {
                        long receivedBlockTimestamp = receivedTerminalBlock.getVerifyingProcessTimeStamp();
                        long currentBlockTimestamp = currentTerminalBlock.getVerifyingProcessTimeStamp();

                        if (receivedBlockTimestamp < currentBlockTimestamp) {
                            Blockchain.blockChain.remove(Blockchain.blockChain.size() - 1);
                            Blockchain.blockChain.add(receivedTerminalBlock);
                        }
                    }
                }

                // If we are in process zero, write block chain to disk
                if (processId == 0) {

                    writeToDisk();

                }

            } catch (Exception e) {
                System.out.println("There was a connection error in BlockChainWorker");
                e.printStackTrace();
            }

        }

        /**
         * Writes current block chain to disk as XML. Cf. BlockRecord class to see XML mappings
         */
        private void writeToDisk() {

            try {

                JAXBContext jaxbContext = JAXBContext.newInstance(BlockRecord.class);
                Marshaller marshaller = jaxbContext.createMarshaller();
                StringWriter stringWriter = new StringWriter();

                // Content is nearly identical to earlier marshalling, but here we set the output to be traditionally
                // formatted so the xml is easy to read
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

                // Marshal each blockRecord in block chain to stringWriter
                for (BlockRecord blockRecord : Blockchain.blockChain) {
                    marshaller.marshal(blockRecord, stringWriter);
                }

                String fullBlockChainLedger = stringWriter.toString();
                String XMLHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
                // Remove duplicate XML headers
                String cleanBlockChainLedger = fullBlockChainLedger.replace(XMLHeader, "");
                // Construct final XML string
                String finalBlockChainLedger = XMLHeader + "\n<BlockLedger>" + cleanBlockChainLedger + "</BlockLedger>";
                // Create new FileWriter instance to write string to disk
                FileWriter fileWriter = new FileWriter("BlockchainLedger.xml");
                // Write to disk
                fileWriter.write(finalBlockChainLedger);
                fileWriter.close();

            } catch (Exception e) {
                System.out.println("There was an error writing the block chain to disk");
                e.printStackTrace();
            }

        }

        /**
         * Sends given block record to Unverified Block Servers
         * @param blockRecordIn a block record to be sent
         */
        private void sendBlockBack (BlockRecord blockRecordIn) {
            Socket sock = null;
            int port;
            PrintStream toServer = null;

            try {

                JAXBContext jaxbContext = JAXBContext.newInstance(BlockRecord.class);
                Marshaller marshaller = jaxbContext.createMarshaller();
                StringWriter stringWriter = new StringWriter();
                marshaller.marshal(blockRecordIn, stringWriter);
                String stringRecordXML = stringWriter.toString();

                for (int i=0; i < Blockchain.numProcesses; i++) {
                    try {
                        int currentPort = Ports.getUnverifiedBlockPortBase() + i;
                        sock = new Socket(Blockchain.serverName, currentPort);
                        toServer = new PrintStream(sock.getOutputStream());
                        toServer.println(stringRecordXML);

                    } catch (Exception e) {
                        System.out.println("There was an issue sending Unverified Block back");
                    } finally {
                        if (sock != null) { try { sock.close(); } catch (Exception e) { e.printStackTrace(); } }
                        if (toServer != null) { toServer.close(); }
                    }
                }

            } catch (Exception e) {
                System.out.println("There was an error marshalling objects");
                e.printStackTrace();
            }
        }
    }


    @Override
    public void run() {

        Socket sock;

        try (ServerSocket servSockBlockchain = new ServerSocket(port, queueLength)) {

            // If we're in final initialized process, send acknowledgement to proceed
            if (processId == Blockchain.numProcesses - 1) {
                Utilities.sendAcknowledgement("block_chain_servers_started");
            }

            while (true) {
                sock = servSockBlockchain.accept();
                new BlockChainWorker(processId, sock).start();
            }
        } catch (Exception e) {
            System.out.println("Error at BlockChainServer");
            e.printStackTrace();
        }
    }
}

/**
 * Server class for processing unverified record blocks
 */
class UnverifiedBlockServer implements Runnable {

    private int processId;
    private int port;
    private int queueLength;

    UnverifiedBlockServer (int processIdIn, int portIn, int queueLengthIn) {
        processId = processIdIn;
        port = portIn;
        queueLength = queueLengthIn;
    }

    /**
     * Inner class to un-marshal unverified block records and add them to the process's
     * priority queue
     */
    class UnverifiedBlockWorker extends Thread {

        private int processId;
        private Socket sock;
        // TODO: Priority queue

        UnverifiedBlockWorker (int processIdIn, Socket sockIn) {
            processId = processIdIn;
            sock = sockIn;
        }

        public void run () {
            try (
                    BufferedReader fromPeer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            ) {

                // Establish context for marshalling with BlockRecord instance
                JAXBContext jaxbContext = JAXBContext.newInstance(BlockRecord.class);
                // Create our unmarshaller
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

                String peerInput;
                peerInput = fromPeer.readLine();

                StringReader stringReader = new StringReader(peerInput);
                // Marshal XML string back into blockRecord
                BlockRecord blockRecord = (BlockRecord) unmarshaller.unmarshal(stringReader);
                // Put our record in priority queue
                Blockchain.unverifiedRecordsQueue.put(blockRecord);
              } catch (Exception e) {
                System.out.println("There was a connection error in UnverifiedBlockWorker");
                e.printStackTrace();
            }
        }

    }

    @Override
    public void run() {

        Socket sock;
        int recordsProcessed = 0;
        try (
                ServerSocket servSockUnverifiedBlocks = new ServerSocket(port, queueLength);

        ) {

            // If we're in final initialized process, send acknowledgement to proceed
            if (processId == Blockchain.numProcesses - 1) {
                Utilities.sendAcknowledgement("block_servers_started");
            }

            while (true) {
                sock = servSockUnverifiedBlocks.accept();
                new UnverifiedBlockWorker(processId, sock).start();
                recordsProcessed++;
                // A bit of a hack, but since we know our initialization size ...
                // Indicates all records have been initialized
                if (recordsProcessed == Blockchain.numProcesses * 4) {
                    System.out.println("Initialization records processed.");
                    // If we are in the final process, send message to main
                    // to proceed
                    if (processId == Blockchain.numProcesses - 1) {
                        Utilities.sendAcknowledgement("init");
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Error at UnverifiedBlockServer");
            e.printStackTrace();
        }

    }
}

/**
 * A serialized container class for sending public keys across network
 */
class KeyContainer implements Serializable {
    private int processId;
    private byte[] key;

    KeyContainer (int processIdIn, byte[] keyIn) {
        processId = processIdIn;
        key = keyIn;
    }

    public int getProcessId() {
        return processId;
    }

    public byte[] getKey() {
        return key;
    }
}


/**
 * A class for generating and holding a process's key pairs.
 */
class KeyHolder {
    private PrivateKey myPrivateKey;
    private PublicKey myPublicKey;

    KeyHolder () {
        generateKeyPair();
    }

    private void generateKeyPair () {

        try {
            // Get key generator instance for specified algorithm
            KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
            // Generate a cryptographically strong random number using specified algorithm
            // Note: SecureRandom will self seed
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            // Randomly intialize the key pair
            keyGenerator.initialize(1024, secureRandom);
            KeyPair keyPair = keyGenerator.generateKeyPair();
            myPrivateKey = keyPair.getPrivate();
            myPublicKey = keyPair.getPublic();

        } catch (Exception e) {
            System.out.println("generateKeyPair () threw an exception. See stack trace: ");
            e.printStackTrace();
        }


    }

    public byte[] getPublicKeyEncoded () {
        return myPublicKey.getEncoded();
    }

    public PrivateKey getMyPrivateKey() {
        return myPrivateKey;
    }

    public PublicKey getMyPublicKey() {
        return myPublicKey;
    }
}

/**
 * Listens for public key multicasts and generate workers upon receipt of communication
 */
class PublicKeyServer implements Runnable {

    private int processId;
    private int port;
    private int queueLength;
    private int keysReceived;

    PublicKeyServer (int processIdIn, int portIn, int queueLengthIn) {
        processId = processIdIn;
        port = portIn;
        queueLength = queueLengthIn;
    }

    public void run () {
        Socket sock;

        try (ServerSocket servSockPublicKeys = new ServerSocket(port, queueLength)) {

            // If we're in final initialized process, send acknowledgement to proceed
            if (processId == Blockchain.numProcesses - 1) {
                Utilities.sendAcknowledgement("key_servers_started");
            }


            while (true) {

                sock = servSockPublicKeys.accept();
                new PublicKeyWorker(processId, sock).start();
                keysReceived ++;
                // Once all the keys have been received
                if (keysReceived == Blockchain.numProcesses) {
                    System.out.println("Keys for initial processes received.");
                    // If we are in the final process, send message to main
                    // to proceed
                    if (processId == Blockchain.numProcesses - 1) {
                        Utilities.sendAcknowledgement("keys_received");
                    }
                }

            }
        } catch (IOException ioe) {
            System.out.println("Problem with public key server");
            ioe.printStackTrace();
        }
    }
}


/**
 * Worker class to process public key requests
 */
class PublicKeyWorker extends Thread {

    private int processId;
    private Socket sock;

    PublicKeyWorker (int processIdIn, Socket sockIn/*, ConcurrentHashMap<Integer, PublicKey> publicKeysIn*/) {
        processId = processIdIn;
        sock = sockIn;
    }

    public void run () {
        try (
                BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                ObjectInputStream objIn = new ObjectInputStream(sock.getInputStream());
        ) {

            // Receive object from stream and cast to KeyContainer
            KeyContainer keyContainer = (KeyContainer) objIn.readObject();
            // Get public key
            byte[] receivedKey = keyContainer.getKey();
            // Get ASN.1 encoding from bytes
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(receivedKey);
            // Get algorithm instance to restore public key
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            // Re-generate public key
            PublicKey restoredPublicKey = keyFactory.generatePublic(keySpec);
            // Put public key in public key hash map
            Blockchain.publicKeys.putIfAbsent(keyContainer.getProcessId(), restoredPublicKey);
        } catch (Exception e) {
            System.out.println("Problem with public key worker");
            e.printStackTrace();
        }
    }
}

/**
 * Listens for initialization messages from UnverifiedBlockServer
 */
class InitializationListener implements Runnable {
    private int port;

    InitializationListener(int portIn) {
        port = portIn;
    }

    @Override
    public void run() {
        int queueLength = 6;
        Socket sock;

        try (ServerSocket serverSocket = new ServerSocket(port)) {

            while (true) {
                sock = serverSocket.accept();
                BufferedReader fromPeer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                String input;
                input = fromPeer.readLine();

                switch (input) {

                    case "key_servers_started": Blockchain.areKeyServersStarted = true;

                    case "keys_received": Blockchain.arePublicKeysReceived = true;

                    case "block_servers_started": Blockchain.areBlockServersStarted = true;

                    case "block_chain_servers_started": Blockchain.areBlockChainServersStarted = true;

                    case "init": Blockchain.isInitialized = true; sock.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Error establishing connection in InitializationListener");
            e.printStackTrace();
        }

    }
}

/**
 * Utility class to generate appropriate port numbers
 */
class Ports {
    private final static int INITIALIZATION_PORT_BASE = 4600;
    private final static int PUBLIC_KEY_PORT_BASE = 4710;
    private final static int UNVERIFIED_BLOCK_PORT_BASE = 4820;
    private final static int UPDATED_BLOCK_CHAIN_PORT_BASE = 4930;

    public int getInitializationPort (int processIdIn) { return INITIALIZATION_PORT_BASE + processIdIn;}

    public int getPublicKeyPort (int processIdIn) {
        return PUBLIC_KEY_PORT_BASE + processIdIn;
    }

    public int getUnverifiedBlockPort(int processIdIn) {
        return UNVERIFIED_BLOCK_PORT_BASE + processIdIn;
    }

    public int getUpdatedBlockChainPort (int processIdIn) {
        return UPDATED_BLOCK_CHAIN_PORT_BASE + processIdIn;
    }

    public static int getInitializationBasePort () {return INITIALIZATION_PORT_BASE;}

    public static int getPublicKeyBasePort() {
        return PUBLIC_KEY_PORT_BASE;
    }

    public static int getUnverifiedBlockPortBase () {
        return UNVERIFIED_BLOCK_PORT_BASE;}

    public static int getUpdatedBlockChainPortBase() {
        return UPDATED_BLOCK_CHAIN_PORT_BASE;
    }
}

/**
 * Basic counter class: used to tabulate how many blocks a process has verified
 */
class Counter {
    private int count;

    Counter() {count = 0;}

    public int getCount () {return count;}

    public void incrementCount() {count++;}
}